#!/usr/bin/env ruby

require 'bundler/setup'
require 'i18n'
require_relative '../config'

require 'nokogiri'
require 'sequel'

require 'optparse'
require 'erb'
require 'fileutils'
require 'set'
require 'digest'

I18n.enforce_available_locales = false

@verbose = false
tmp_dir  = '/tmp'
pdf_dir  = '.'
md5_dir  = '.'

opt_parser = OptionParser.new do |opt|
  opt.banner = 'Usage: cdc-index-tex [opt] <year>'
  opt.separator  ''

  opt.on('-m', '--md5-dir [DIR]',
         'MD5 output directory') do |dir|
    md5_dir = dir
  end

  opt.on('-p', '--pdf-dir [DIR]',
         'pdf output directory') do |dir|
    pdf_dir = dir
  end

  opt.on('-t', '--tmp-dir [DIR]',
         'TeX temporary directory') do |dir|
    tmp_dir = dir
  end

  opt.on('-v', '--verbose', 'noisy operation') do
    @verbose = true
  end
end

opt_parser.parse!

if ARGV.count != 1 then
  puts opt_parser.help
  exit(1)
end

year = ARGV.first.to_i

def info(*args)
  if @verbose then
    puts *args
  end
end

info 'This is cdc-index-tex'
info 'sqlite in %s' % cdc_path_db

require_cdc 'db'
require_cdc 'model'

file_base = 'CdC-%i' % year
path_ind = '%s/%s.ind' % [tmp_dir, file_base]
path_tex = '%s/%s.tex' % [tmp_dir, file_base]
path_rev = '%s/%s.rev' % [tmp_dir, file_base]
path_pdf = '%s/%s.pdf' % [tmp_dir, file_base]

info 'creating %s' % path_ind

def page_range(first, last)
  if first == last
    '%i' % first
  elsif first < last
    '%i--%i' % [first, last]
  else
    raise ArgumentError, "bad page range #{first}-#{last}"
  end
end

def page_text(review)
  page_range(review.page_first, review.page_last)
end

def initialised(reviewer)
  reviewer.initials.gsub(/ /, '\,')
end

def reviewers_text(review)
  review
    .reviewers
    .sort_by(&:family)
    .map { |reviewer| initialised(reviewer) }
    .join(', ')
end

def review_text(review)
  [reviewers_text(review), page_text(review)].join(' ')
end

def reviews_text(reviews)
  reviews
    .sort_by(&:page_first)
    .map { |review| review_text(review) }
    .join(', ')
end

def film_sort_key(film)
  @filmno ||= 0
  @filmno += 1
  film.sort_key(@filmno)
end

def title_definitive(films)
  titles = films.map(&:title).uniq
  if titles.length != 1 then
    msg = "multiple titles: #{titles.join(', ')}"
    raise RuntimeError, msg
  end
  titles.first
end

def build_entry(reviews, films)
  films_definitive =
    if films.count != 1 then
      films.select { |film| film.title_definitive }
    else
      films
    end
  title = title_definitive(films_definitive)
  film = films_definitive.first
  reviews_by_issue = reviews.each_with_object(Hash.new) do |review, result|
    (result[review.issue] ||= []) << review
  end
  issues = reviews_by_issue.keys.sort_by(&:number)
  issues_texts = issues.map do |issue|
    format(
      '\textbf{%s} %s',
      issue.number,
      reviews_text(reviews_by_issue[issue])
    )
  end
  issues_text = issues_texts.join(' ; ')
  entry = format(
    '\item \textit{%s} %s \dotfill %s',
    title,
    film.directors_attribution,
    issues_text
  )
  entry.gsub!(/ł/, '\l{}')
  entry.gsub!(/ᵉ/, '\textsuperscript{e}')
  entry.gsub!(/ᵒ/, '\textsuperscript{o}')
  entry.gsub!(/ᵗʰ/, '\textsuperscript{th}')
  entry.gsub!(/&/, '\\\&')
  entry.gsub!(/’/, '{\'}')
  entry.gsub!(/—/, '---')
  entry.gsub!(/–/, '--')
  entry.gsub!(/œ/, '\oe{}')
  entry.gsub!(/Œ/, '\OE{}')
  entry.gsub!(/%/, '\%')
  entry.gsub!(/…/, '\ldots')
  entry.gsub!(/\$/, '\textdollar')
  entry.gsub!(/ğ/, '\breve{g}')
  entry.gsub!(/Ž/, '\v{Z}')
  entry.gsub!(/ū/, '\=u')
  entry.encode!(Encoding::ISO_8859_1, undef: :replace)
end

# the key mapping reviewer initials to their full name (and
# if that name is an alias, then the real name parentetically)
# which is set as a footnote on the first page, \input by the
# main TeX file ...

info "creating #{path_rev}"

File.open(path_rev, "w:iso8859-1") do |stream|
  reviewer_hash = Hash.new
  CdC::Issue.filter(year: year).each do |issue|
    issue.reviews.each do |review|
      review.reviewers.each do |reviewer|
        key = AnyAscii.transliterate(reviewer.initials).downcase
        reviewer_hash[key] ||= (
          '%s, %s' % [
            initialised(reviewer),
            if reviewer.aliased then
              '%s (%s)' % [
                reviewer.full_name,
                reviewer.aliased.full_name
              ]
            else
              reviewer.full_name
            end
          ]
        ).gsub(/’/, '{\'}')
      end
    end
  end
  reviewers_text =
    reviewer_hash
      .keys
      .sort
      .map { |key| reviewer_hash[key] }
      .join(" ;\n")
  stream.puts reviewers_text
end

# the ind file which makes-up the index

File.open(path_ind, "w:iso8859-1") do |stream|

  stream.puts '\begin{theindex}'

  review_hash = Hash.new
  film_hash = Hash.new
  sort_hash = Hash.new

  CdC::Issue.filter(year: year).each do |issue|
    issue.reviews.each do |review|
      review.films.each do |film|
        slug = film.slug
        sort_key = film_sort_key(film)
        (review_hash[slug] ||= []) << review
        (film_hash[slug] ||= []) << film
        sort_hash[slug] = sort_key
      end
    end
  end

  entry_hash = Hash[
    sort_hash.keys.map do |slug|
      entry = build_entry(review_hash[slug], film_hash[slug])
      entry.gsub!(/&amp;/, '\&')
      [sort_hash[slug], entry]
    end
  ]

  groups = entry_hash.keys.group_by { |key| key[0] }
  groups.keys.sort.each do |group_key|
    groups[group_key].sort.each do |key|
      stream.puts entry_hash[key] + "\n"
    end
    stream.puts '\indexspace'
  end

  stream.puts '\end{theindex}'
end

# read the ind file, see if anything has changed, if not
# then we skip generating the PDF file -- we use the ind
# as a proxy for the PDF input TeX (which includes the ind),
# the TeX actually changes very rarely (and one could 'flush'
# by deleting everthing in var/md5 to manually regenerated
# PDF in that case)

ind_md5 = Digest::MD5.hexdigest(IO.binread(path_ind))
path_ind_md5 = "#{md5_dir}/#{file_base}.ind.md5"

if File.exist? path_ind_md5 then
  ind_md5_previous = File.read(path_ind_md5)
  if ind_md5 == ind_md5_previous then
    info "MD5 match (#{ind_md5}), no index change"
    info 'done.'
    exit 0
  else
    info "updating #{path_ind_md5}"
    File.open(path_ind_md5, 'w') { |stream| stream.write(ind_md5) }
  end
else
  info "creating #{path_ind_md5}"
  File.open(path_ind_md5, 'w') { |stream| stream.write(ind_md5) }
end

# create TeX

class TeX < ERB

  def self.template
    File.read(cdc_path_index_year_template, encoding: 'iso-8859-1')
  end

  def initialize(year, options = Hash.new)
    @year = year
    @template = options.fetch(:template, self.class.template)
    super(@template)
  end

  def result
    super(binding)
  end
end

info 'creating %s' % path_tex

File.open(path_tex, "w:iso8859-1") do |stream|
  stream.puts(TeX.new(year).result)
end

Dir.chdir(tmp_dir) do
  system('pdflatex', '-interaction=batchmode', file_base)
end

FileUtils.cp(path_pdf, pdf_dir)

info 'copied %s to %s' % [path_pdf, pdf_dir]

info 'done.'
