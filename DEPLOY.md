Deployment
----------

Assuming a git clone is installed in

    /usr/local/share/cdc-index

and one wants to serve the API from `/pub/cdc` with Apache
Passenger 4, add the following

    PassengerRuby /usr/bin/ruby
    PassengerUserSwitching on
    PassengerUser www-data
    PassengerGroup www-data

    Alias /pub/cdc /usr/local/share/cdc-index/api
    <Location /pub/cdc>
        PassengerBaseURI /pub/cdc
        PassengerAppRoot /usr/local/share/cdc-index
    </Location>
    <Directory /usr/local/share/cdc-index/api>
        Allow from all
        Options -MultiViews
        Require all granted
    </Directory>

to the Apache configuration, typically

	/etc/apache2/sites-enabled/000-default.conf

restart Apache with

	service apache2 restart

To test, visit

	http://localhost/pub/cdc/api/imdb/tt2402927

which should display some JSON.

Updating
--------

Run

	ssh -A <host>

	cd /usr/local/share/cdc-index
	git pull
	bundle install --deployment
	make db
	sudo /etc/init.d/apache2 restart
