Contributing
------------

There are a lot of issues missing from the index, if you have
some (and have the time), contributions would be most welcome.


### Create the XML file for the issue

The index source is a collection of XML files, one for each issue,
in the [src/issue][1] directory.  Each file is named after the
number of issue, padded out to three figures (as `001.xml`) and
looks like

```xml
<?xml version="1.0" encoding="utf-8"?>
<cdc:issue xmlns:cdc="https://jjg.gitlab.io/ns/cdc/0.3/">

  <number>1</number>
  <year>1951</year>
  <month>4</month>
  <file>n1-avril-1951</file>

  <!-- films -->

</cdc:issue>
```

The `number`, `year` and `month` are obvious, possibly the issue
covers a range of months in which case the latter is replaced by

```xml
<month_first>7</month_first>
<month_last>8</month_last>
```

The `file` value is the name of the page on the _Cahiers_ website
which lists the issue (the final part of the path), so

```xml
<file>n1-avril-1951</file>
```

gives the URL

```
https://www.cahiersducinema.com/boutique/produit/n1-avril-1951/
```

which links [here][2].

The reviews are the entries in the file, quite often there are
multiple reviews for a single film.  In this case an entry can be
for the film:

```xml
<film>
  <title lang='en'>Master Gardener</title>
  <director>Paul Schrader</director>
  <imdb>tt15342244</imdb>
  <aoc>296091</aoc>
  <review type='long'>
    <page_first>68</page_first>
    <page_last>69</page_last>
    <reviewer>Yal Sadat</reviewer>
  </review>
  <review type='interview'>
    <page>70</page>
    <reviewer>Olivia Cooper-Hadjian</reviewer>
  </review>
</film>
```

We only include reviews of films which are released to the cinema,
this does not include VHS tapes, DVDs, or streaming (Netflix etc.)
releases.

The `title` should the title of the film as given in the review,
so the same language, the same punctuation etc.  The only exception
is when there is an obvious spelling mistake (this happens in a few
of the English titles in the early issues). The `lang` attribute
gives the language of the title text (not the film), this is needed
since we want to ignore the definite article ("The") when sorting
the films, and those depend on the language.  We use [ISO 639-1][3]
codes for this.  Occasionally, there are multiple reviews of the
same film with different titles, these are handled with the `status`
attribute:

- One title in the original language (`definitive`), one in French
  (`alternative`)
- An early pre-release title (`provisional`) and a (usually similar)
  release  title (`definitive`)
- A release title (`definitive`) and a re-release title
  (`alternative`)

The `director` is the name of the director as given in the review,
if there are multiple directors then have one `director` for each:

```xml
<director>Jean Grémillon</director>
<director>Pierre Kast</director>
```

The `imdb` value is as used on the URL for the film on IMBb, so
that `tt15342244` gives us the URL

```
https://www.imdb.com/title/tt15342244
```

which links [here][4].  Similarly, the (optional) `aoc` value is
as used in the URL for the film on Allocine, so `296091` gives

```
https://www.allocine.fr/film/fichefilm_gen_cfilm=296091.html
```

which links [here][5].

Then a `review` entry for each review.  The `type` attribute is
usually

- `event`, usually the film on the cover of the issue, or at
  least before the _Cahier critiques_ section,
- `long`, the longer reviews in the _Cahier critiques_ section
  proper, typically a page or two,
- `interview`, often accompanying a review (and in the same
  entry)
- `note`, short reviews in the _Notes sur d’autres films_ section,
  usually 3–4 of them per page.

There are a few other types, the full list is in the [schema][6].

Occasionally, one finds a single review which refers to multiple
films, in this case one can create a `review` entry:

```xml
<review type='long'>
  <film>
    <title lang='en'>The Teahouse of the August Moon</title>
    <director>Daniel Mann</director>
    <imdb>tt0049830</imdb>
  </film>
  <film>
    <title lang='en'>The Bachelor’s Party</title>
    <director>Delbert Mann</director>
    <imdb>tt0050156</imdb>
  </film>
  <page_first>56</page_first>
  <page_last>58</page_last>
  <reviewer>Claude de Givray</reviewer>
</review>
```

Even rarer, episodic works: a single (usually long) film broken
into multiple parts (episodes).  Then one _can_ make a film entry,
but need to add the `episode` attribute to the `imdb` and `aoc`
elements:

```xml
<film>
  <title lang='fr'>Les Mille et Une Nuits</title>
  <director>Miguel Gomes</director>
  <imdb episode='1'>tt3284178</imdb>
  <imdb episode='2'>tt4692234</imdb>
  <imdb episode='3'>tt4692242</imdb>
  <aoc episode='1'>237368</aoc>
  <aoc episode='2'>237369</aoc>
  <aoc episode='3'>237370</aoc>
  <review type='event'>
    <page_first>24</page_first>
    <page_last>27</page_last>
    <reviewer>Joachim Lepastier</reviewer>
  </review>
  <review type='interview'>
    <page_first>28</page_first>
    <page_last>32</page_last>
    <reviewer>Nicholas Elliott</reviewer>
    <reviewer>Joachim Lepastier</reviewer>
  </review>
</film>
```

### Add the XML file to the index

If you're happy with "git workfow" and have the development tools
then you can add the new XML file to the index; if you're not, no
problem, just email it [to me][7].

First, there is a script which checks for a few common typos,
it checks all files in `src/issue`, just run

    make typolint

it does need you to have the `xml2` program installed.

Next, add the issue number to the file `src/completed`, that's
just a list of the issue numbers (in order). Then

    make validate

which checks all completed XML files against the XML schema, that
requires the `xmlstarlet` program is installed.

Creation of the DB with the new entry is done with

    make db

That's a Ruby script, and uses several gems (Ruby components), so
you'll need Ruby installed and you'll have to run

    bundle install

to install those gems.

Now create a new branch, add your changes to it and create a pull
request, and many thanks.


[1]: https://gitlab.com/jjg/cdc-index/-/tree/master/src/issue
[2]: https://www.cahiersducinema.com/boutique/produit/n1-avril-1951/
[3]: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
[4]: https://www.imdb.com/title/tt15342244/
[5]: https://www.allocine.fr/film/fichefilm_gen_cfilm=296091.html
[6]: https://gitlab.com/jjg/cdc-index/-/blob/master/lib/cdc.xsd#L117
[7]: mailto:j.j.green@gmx.co.uk?subject=CdC%20contribution
