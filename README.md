Cahiers du Cinéma, index of reviews
-----------------------------------

A partial index of the reviews of _Cahiers du Cinéma_ which can be
accessed

- with a simple REST API
- in pdf format
- in BibTeX format
- as HTML

For further information see the [project page][2].

Note that the _Cahiers_ reviews will not be included here, this is
exclusively an index.

[1]: https://addons.mozilla.org/en-US/firefox/addon/cdc-index/
[2]: https://jjg.gitlab.io/en/code/cdc-index/
