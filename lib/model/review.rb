class CdC::Review < Sequel::Model(:reviews)

  many_to_many(:reviewers, join_table: :reviewer_reviews)
  many_to_many(:films, join_table: :film_reviews)
  many_to_one(:issue, key: :number)

  plugin :validation_helpers

  def validate
    super
  end

end
