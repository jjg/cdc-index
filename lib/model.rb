require 'sequel'

module CdC ; end

require_relative 'model/film'
require_relative 'model/imdb'
require_relative 'model/aoc'
require_relative 'model/review'
require_relative 'model/reviewer'
require_relative 'model/issue'
