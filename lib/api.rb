require 'grape'

module CdC
  class API < Grape::API

    helpers do

      def not_found
        error!('Not found', 404)
      end

      def sorted_reviews(film)
        film.reviews.sort_by do |review|
          [review.issue.number, review.page_first]
        end
      end

      def film_data(films)
        films.each_with_object(Array.new) do |film, result|
          sorted_reviews(film).each do |review|
            issue = review.issue
            result << {
              issue: issue.number,
              year: issue.year,
              url_base: issue.file,
              pages: {
                first: review.page_first,
                last: review.page_last,
              },
              reviewers: review.reviewers.map do |reviewer|
                if reviewer.aliased then
                  format(
                    '%s (%s)',
                    reviewer.full_name,
                    reviewer.aliased.full_name,
                  )
                else
                  reviewer.full_name
                end
              end
            }
          end
        end
      end

    end

    version('v1', using: :header, vendor: 'jjg')
    format(:json)
    prefix(:api)

    resource(:status) do

      desc 'status route'
      get do
        { status: 'ok' }
      end
    end

    resource(:imdb) do

      desc 'by IMDB id'
      params do
        requires(:id, type: String, desc: 'IMDB id')
      end

      route_param(:id) do
        get do
          films = IMDb.where(imdb_id: params[:id]).map(&:film)
          not_found if films.empty?
          film_data(films)
        end
      end
    end

    resource(:aoc) do

      desc 'by AlloCiné id'
      params do
        requires(:id, type: String, desc: 'AlloCiné id')
      end

      route_param(:id) do
        get do
          films = AoC.where(aoc_id: params[:id]).map(&:film)
          not_found if films.empty?
          film_data(films)
        end
      end
    end

    route :any, '*path' do
      error! 'Not there', 404
    end
  end
end
