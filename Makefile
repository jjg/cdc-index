# Makefile for cdc

YEARS_FULL = 1951 1952 1953 1954 1955 1956 1957 1958 1959 \
	1960 1961 1962 1963 2013 2014 2015 2016 2017 2018 \
	2019 2020 2021 2022 2023 2024
YEARS_PART = 2025
YEARS = $(YEARS_FULL) $(YEARS_PART)

validate:
	bin/cdc-validate-completed

db:
	bin/cdc-db-create -v

pdf: pdf-index pdf-bib

pdf-index:
	for year in $(YEARS) ; do \
	  bin/cdc-index-tex -v -p var/pdf -m var/md5 $$year ; \
	done

pdf-bib:
	for year in $(YEARS_FULL) ; do \
	  bin/cdc-bib-year -v -p var/pdf $$year ; \
	done

html: html-reviewers html-index

html-reviewers:
	bin/cdc-reviewers-html -v -H var/html

html-index:
	for year in $(YEARS) ; do \
	  bin/cdc-index-html -v -H var/html $$year ; \
	done

bib:
	for year in $(YEARS_FULL) ; do \
	  bin/cdc-index-bibtex -v -o var/bib/cdc$$year.bib $$year ; \
	done

bib-zip:
	bin/cdc-bib-zip var/bib var/cdc-bib.zip

typolint:
	bin/cdc-typolint src

plugin-tag:
	bin/cdc-plugin-git-release-tag

api-specs:
	bundle exec rake spec

.PHONY: validate db imdb pdf html
