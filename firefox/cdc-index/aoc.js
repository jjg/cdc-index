/* exported apiUrl */
/* global getCahiers */

function apiUrl(aocId) {
    return 'http://miles.shef.ac.uk/pub/cdc/api/aoc/' + aocId;
}

function aocId() {
    var re = /fichefilm_gen_cfilm=(\d+)\.html/;
    return document.location.pathname.match(re)[1];
}

function main() {
    getCahiers(aocId());
}

main();
