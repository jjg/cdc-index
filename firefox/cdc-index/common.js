/* exported makeIssueNodes getCahiers */
/* global addCahiers apiUrl */

/*
  Get Cahiers data for film with specified id from the
  API served from apiUrl(id)
*/

function getCahiers(id) {
    var req = new XMLHttpRequest();
    req.open('GET', apiUrl(id), true);
    req.setRequestHeader('Content-type', 'application/json');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            var response = [];
            if (req.status === 200) {
                response = JSON.parse(req.responseText);
            }
            addCahiers(response);
        }
    };
    req.send();
}

/*
  urlBase is untrusted since received from remote server,
  it should be an ASCII string such as "janvier-2016-n718"
  which is unaffected by URI component-encoding, but the
  same (since encoding ":") should suffice to make the return
  of this function non-executable in case the remote server is
  compromised.
*/

function issueUrl(urlBase) {
    if (urlBase) {
        var urlBaseEncoded = encodeURIComponent(urlBase);
        return 'https://www.cahiersducinema.com/boutique/produit/' + urlBaseEncoded + '/';
    }
    return null;
}

/* string helpers */

function commaJoin(strings) {
    return strings.join(', ');
}

function numberRange(n, m) {
    return n + '–' + m;
}

/*
  return an array with, between each pair of elements, a fresh
  text node of the specified text
*/

function insertTextNodes(inputArray, text) {
    var len = inputArray.length;
    if (len === 0) {
        return [];
    }
    var outputArray = [inputArray[0]];

    var i;
    for (i = 1 ; i < len ; i += 1) {
        outputArray.push(document.createTextNode(text));
        outputArray.push(inputArray[i]);
    }
    return outputArray;
}

/* flattens a nested array */

function flatten(array) {
    return array.reduce(function (flat, toFlatten) {
        return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
    }, []);
}

/* parenthesise a node, returns an array of nodes */

function parenthesiseNode(node) {
    return [
        document.createTextNode('('),
        node,
        document.createTextNode(')')
    ];
}

/*
  returns node <span title='reviewerText'>pageText</span>, so
  the reviewers appear as an HTML tooltip (browser willing)
*/

function makePageNode(pageText, reviewerText) {
    var node = document.createElement('span');
    node.textContent = pageText;
    node.setAttribute('title', reviewerText);
    return node;
}

function makePageText(pages) {
    var pageText;
    if (pages.first === pages.last) {
        pageText = pages.first;
    } else {
        pageText = numberRange(pages.first, pages.last);
    }
    return pageText;
}

function makeReviewerText(reviewers) {
    return commaJoin(reviewers);
}

/*
  return node <a href='url'>issueText</a> (if url) otherwise
  just the issueText
*/

function makeIssueNode(issueText, url, urlClass) {
    var node;
    if (url) {
        node = document.createElement('a');
        node.textContent = issueText;
        node.setAttribute('href', url);
        if (urlClass) {
            node.className = urlClass;
        }
    } else {
        node = document.createTextNode(issueText);
    }
    return node;
}

/* convert the result set to an array of nodes */

function makeIssueNodes(results, urlClass) {
    var pageNodes = {}, urls = {};

    results.forEach(
        function(result) {
            var pageNode = makePageNode(
                makePageText(result.pages),
                makeReviewerText(result.reviewers)
            );
            var issue = result.issue;
            if (pageNodes.hasOwnProperty(issue)) {
                pageNodes[issue].push(pageNode);
            } else {
                pageNodes[issue] = [pageNode];
            }
            if (! urls.hasOwnProperty(issue)) {
                urls[issue] = issueUrl(result.url_base);
            }
        }
    );

    var issues = Object.keys(pageNodes).sort(
        function(a, b){ return a - b; }
    );

    var issueNodes = issues.map(
        function(issue) {
            var issueNode = makeIssueNode(issue, urls[issue], urlClass);
            var spaceNode = document.createTextNode(' ');
            return flatten([
                issueNode,
                spaceNode,
                parenthesiseNode(insertTextNodes(pageNodes[issue], ', '))
            ]);
        }
    );

    issueNodes = flatten(insertTextNodes(issueNodes, ' ; '));

    return issueNodes;
}
