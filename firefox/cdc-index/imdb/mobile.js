/* exported addCahiers */
/* global makeIssueNodes */

function addCahiers(results) {

    var cahiersTitle = document.createElement('div');
    cahiersTitle.textContent = 'Cahiers du Cinéma';

    var cahiersData = document.createElement('small');

    if (results.length > 0) {
        makeIssueNodes(results, null).forEach(
            function(issueNode) {
                cahiersData.appendChild(issueNode);
            }
        );
    } else {
        cahiersData.textContent = 'Rien n’a été trouvé';
    }

    var cahiersSpan = document.createElement('span');
    cahiersSpan.className = 'inline-block text-left vertically-middle';

    cahiersSpan.appendChild(cahiersTitle);
    cahiersSpan.appendChild(cahiersData);

    var cahiersItem = document.createElement('div');
    cahiersItem.className = 'inline-block text-centre';

    cahiersItem.appendChild(cahiersSpan);

    var ratingsBar = document.getElementById('ratings-bar');
    if (ratingsBar === null) {
        var message = 'CdC index: No ratings-bar found';
        throw new Error(message);
    }

    ratingsBar.appendChild(cahiersItem);
}
