/* exported addCahiers */
/* global makeIssueNodes */

function addCahiers(results) {

    var divider = document.createElement('div');
    divider.className = 'divider';

    var cahiersTitle = document.createElement('div');
    cahiersTitle.textContent = 'Cahiers du Cinéma';

    var cahiersContentSpan = document.createElement('span');
    cahiersContentSpan.className = 'subText';

    if (results.length > 0) {
        makeIssueNodes(results, null).forEach(
            function(issueNode) {
                cahiersContentSpan.appendChild(issueNode);
            }
        );
    } else {
        cahiersContentSpan.textContent = 'Rien n’a été trouvé';
    }

    var cahiersContentDiv = document.createElement('div');
    cahiersContentDiv.appendChild(cahiersContentSpan);

    var cahiersItem = document.createElement('div');
    cahiersItem.className = 'titleReviewBarItem';

    cahiersItem.appendChild(cahiersTitle);
    cahiersItem.appendChild(cahiersContentDiv);

    var titleReviewBars = document.getElementsByClassName('titleReviewBar');
    if (titleReviewBars.length !== 1) {
        var message = 'Found ' + titleReviewBars.length + ' titleReviewBars';
        throw new Error(message);
    }
    var titleReviewBar = titleReviewBars[0];

    titleReviewBar.appendChild(divider);
    titleReviewBar.appendChild(cahiersItem);
}
